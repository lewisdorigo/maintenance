<?php namespace Dorigo\Maintenance;

use \Dorigo\Singleton\Singleton;

class Maintenance extends Singleton {

    private $active;
    private $installedDirectory;
    private $templateFile = "maintenance.php";

    protected function __construct() {
        $this->installedDirectory = dirname(__DIR__);

        add_action("init", [$this, "setupRewrites"]);
        add_action("template_redirect", [$this, "templateRedirect"], 8);
        add_action("template_include", [$this, "templateInclude"], 8);

        add_action("Dorigo\Maintenance", [$this,"doMaintenance"]);

        add_filter("wpseo_title",[$this,"seoTitle"], 100);
        add_filter("wpseo_metadesc",[$this,"seoDescription"], 100);

        add_action("wp_enqueue_scripts", [$this, "removeScripts"], 1000);

        add_filter("woocommerce_ga_disable_tracking", function() { return true; });
    }

    public function setupRewrites() {
        global $wp_rewrite;

        $rewrite_rules = get_option("rewrite_rules");

        add_filter("query_vars", function($qv) {
            $qv[] = "drgo_maintenance";
            return $qv;
        });

        add_rewrite_rule("maintenance\/?", $wp_rewrite->index."?drgo_maintenance=1", "top");

        if(!isset($rewrite_rules["maintenance\/?"])) {
            flush_rewrite_rules(false);
        }
    }

    public function removeScripts() {
        if($this->isMaintenance()) {

            wp_deregister_script("polyfill");
            wp_deregister_script("modernizr");
            wp_deregister_script("app-js");
            wp_deregister_script("jquery-core");
            wp_deregister_script("wp-embed");

        }
    }

    public function templateRedirect() {
        if($this->isMaintenance()) {
            do_action("Dorigo\Maintenance");
        }
    }

    public function templateInclude($main) {
        if($this->isMaintenance()) {
            return $this->templateFile;
        }

        return $main;
    }

    public function isMaintenance() {
        if(is_null($this->active)) {
            $this->active = (bool) get_query_var("drgo_maintenance");
        }

        return $this->active;
    }

    public function doMaintenance() {
    	$protocol = wp_get_server_protocol();

    	header("{$protocol} 503 Service Unavailable", true, 503);
    	header("Content-Type: text/html; charset=utf-8");
    	header("Retry-After: 600");


    	header("X-Robots-Tag: noarchive, noindex, nofollow, noodp");

    	define("DONOTCACHEPAGE", true);

    	if(!locate_template($this->templateFile)) {
        	require $this->installedDirectory."/templates/maintenance.php";
        	die();
    	}
    }

    public function seoTitle($title) {
        if($this->isMaintenance()) {
            return "We’re down for maintenance and will be back soon - ".get_bloginfo('name');
        }

        return $title;
    }

    public function seoDescription($desc) {
        if($this->isMaintenance()) {
            return "We’re down for maintenance and will be back soon";
        }

        return $desc;
    }
}