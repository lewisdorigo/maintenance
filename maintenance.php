<?php
/**
 * Plugin Name:       Maintenance
 * Plugin URI:        https://dorigo.co/
 * Description:       Allows a maintenance mode to be set.
 * Version:           1.0.1
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

if(!defined('ABSPATH')) { die(); }

if(file_exists(__DIR__."/vendor") && is_dir(__DIR__."/vendor")) {
    require(__DIR__."/vendor/autoload.php");
}

require(__DIR__."/lib/Maintenance.php");


\Dorigo\Maintenance\Maintenance::getInstance();